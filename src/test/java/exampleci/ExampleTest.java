package exampleci;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ExampleTest {

	@Test
	public void sayHelloTest() {
		assertEquals("Hello World", new Example().sayHello());
	}

}
